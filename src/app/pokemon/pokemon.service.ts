import { Injectable } from '@angular/core';
import {Pokemon} from "./pokemon";
import {POKEMONS} from "./pokemonList";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable() // j'ai enlever le pokemon providers in : 'root' qui est d'habitude la pour l'injecté dans le pokemon module
export class PokemonService {

  constructor(private http: HttpClient) {
  }
  //pokemon1:Pokemon;

  getPokemonList(): Observable<Pokemon[]>{ //données synchrone on recoit une liste de pokemon mais en un temps
    return this.http.get<Pokemon[]>('api/pokemons')
  }
  /*getPokemonList(): Pokemon[]{
    return POKEMONS;
  }*/

  /*async getMeteoData(): Promise<any>{
    return this.http.get<any>('https://www.7timer.info/bin/astro.php?lon=113.2&lat=23.1&ac=0&unit=metric&output=json&tzshift=0')
      .toPromise()
      .then((result) => {return result});
  }*/

  getPokemonById(pokemonId: number): Pokemon|undefined{
    return POKEMONS.find(pokemon => pokemon.id == pokemonId);
    /*return POKEMONS.find(function(pokemon) {
      return pokemon.id == pokemonId;
    });*/
  }

  getPokemonTypesList(): string[]{
    let pokemonTypes: string[] = [];
    POKEMONS.forEach(pokemonn => {
      pokemonn.types.forEach(type => {
        if(pokemonTypes.indexOf(type) === -1){
          pokemonTypes.push(type)
        }
      })
    })
    return pokemonTypes;
    //return ['feu'];
  }
}
